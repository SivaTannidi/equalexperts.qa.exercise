﻿using EqualExperts.Hotel.Tests.Base;
using EqualExperts.Hotel.Tests.Utils;
using System;
using TechTalk.SpecFlow;

namespace EqualExperts.Hotel.Tests.Hooks
{
    [Binding]
    public class TestInitializeHook
    {
        [BeforeFeature]
        public static void StartTests()
        {
            BrowserFactory.OpenBrowser(ConfigUtils.LoadFromConfig().Browser);
            DriverFactory.Driver.Manage().Window.Maximize();
            DriverFactory.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        }

        [AfterFeature]
        public static void StopTests()
        {
            DriverFactory.Driver.Quit();
        }
    }
}
