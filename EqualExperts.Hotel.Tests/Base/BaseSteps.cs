﻿using EqualExperts.Hotel.Tests.Pages;

namespace EqualExperts.Hotel.Tests.Base
{
    public abstract class BaseSteps : DriverFactory
    {
        public HotelBookingFormPage hotelBookingFormPage = new HotelBookingFormPage();
    }
}
