﻿using OpenQA.Selenium;

namespace EqualExperts.Hotel.Tests.Base
{
    public abstract class DriverFactory
    {
        private static IWebDriver _driver;

        public static IWebDriver Driver
        {
            get
            {
                return _driver;
            }
            set
            {
                _driver = value;
            }
        }

        public static BrowserFactory Browser { get; set; }
    }
}
