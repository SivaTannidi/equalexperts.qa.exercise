﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace EqualExperts.Hotel.Tests.Base
{
    public class BrowserFactory
    {
        private readonly IWebDriver _driver;
        private static DesiredCapabilities cap;

        public BrowserFactory(IWebDriver driver)
        {
            _driver = driver;
        }

        public BrowserType Type { get; set; }

        public void GoToUrl(string url)
        {
            DriverFactory.Driver.Url = url;
        }

        public static void OpenBrowser(BrowserType browserType)
        {
            switch (browserType)
            {
                case BrowserType.Chrome:
                    DriverFactory.Driver = new ChromeDriver();
                    DriverFactory.Browser = new BrowserFactory(DriverFactory.Driver);
                    break;
                case BrowserType.FireFox:
                    DriverFactory.Driver = new FirefoxDriver();
                    DriverFactory.Browser = new BrowserFactory(DriverFactory.Driver);
                    break;
                case BrowserType.Ie:
                    DriverFactory.Driver = new InternetExplorerDriver();
                    DriverFactory.Browser = new BrowserFactory(DriverFactory.Driver);
                    break;
            }

        }
    }

    public enum BrowserType
    {
        Chrome,
        FireFox,
        Ie
    }
}
