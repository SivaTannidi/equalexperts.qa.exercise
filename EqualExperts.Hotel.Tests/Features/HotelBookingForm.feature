﻿Feature: HotelBookingForm
	Create one or more automated tests to check the creation and deletion of bookings.

Background:
	Given I am on the Hotel Booking Form website
	And I see the booking form

Scenario Outline: Create a new successfull booking and delete it
	When I fill in <Firstname>, <Surname> and <Price>
	And I select <Deposit> from Deposit dropdown
	And I select <Checkin> and <Checkout> dates
	Then I click Save button to create new booking
	And I click Delete button for the last booking in the table to delete the last booking in the table

Examples:
| Firstname | Surname | Price | Deposit | Checkin    | Checkout   |
| First     | Sur     | 1     | true    | 2018-07-03 | 2018-07-05 |
| 123       | 456     | 1.1   | false   | 2018-05-03 | 2018-06-05 |