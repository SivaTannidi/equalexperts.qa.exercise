﻿using EqualExperts.Hotel.Tests.Base;
using OpenQA.Selenium.Support.UI;
using System;

namespace EqualExperts.Hotel.Tests.Extensions
{
    public static class WaitExtensions
    {
        public static WebDriverWait Wait()
        {
            return new WebDriverWait(DriverFactory.Driver, TimeSpan.FromSeconds(30));
        }
    }
}
