﻿using EqualExperts.Hotel.Tests.Base;
using System;
using System.Configuration;

namespace EqualExperts.Hotel.Tests.Utils
{
    public class ConfigUtils
    {
        public BrowserType Browser { get; set; }
        public string BaseUrl { get; set; }

        public static ConfigUtils LoadFromConfig()
        {
            var browser = ConfigurationManager.AppSettings["Browser"];
            BrowserType browserType;
            var baseUrl = ConfigurationManager.AppSettings["BaseUrl"];

            if (!Enum.TryParse<BrowserType>(browser, true, out browserType))
            {
                throw new ConfigurationErrorsException("No value was supplied for key Browser");
            }

            if (string.IsNullOrWhiteSpace(baseUrl))
            {
                throw new ConfigurationErrorsException("No value was supplied for key BaseUrl");
            }

            return new ConfigUtils()
            {
                Browser = browserType,
                BaseUrl = baseUrl,
            };
        }
    }
}
