﻿using EqualExperts.Hotel.Tests.Base;
using EqualExperts.Hotel.Tests.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

using static EqualExperts.Hotel.Tests.Utils.ConfigUtils;

namespace EqualExperts.Hotel.Tests.Steps
{
    [Binding]
    public sealed class HotelBookingFormSteps : BaseSteps
    {
        [Given(@"I am on the Hotel Booking Form website")]
        public void GivenIAmOnTheHotelBookingFormWebsite()
        {
            Driver.Navigate().GoToUrl(LoadFromConfig().BaseUrl);
        }

        [Given(@"I see the booking form")]
        public void GivenISeeTheBookingForm()
        {
            hotelBookingFormPage.IsBookingFormDisplayed();
        }

        [When(@"I fill in (.*), (.*) and (.*)")]
        public void WhenIFillInFirstnameSurnameAndPrice(string firstName, string surName, string price)
        {
            hotelBookingFormPage.FillFirstName(firstName);
            hotelBookingFormPage.FillSurName(surName);
            hotelBookingFormPage.FillPrice(price);
        }

        [When(@"I select (.*) from Deposit dropdown")]
        public void WhenISelectValueFromDepositDropdown(string isDeposit)
        {
            hotelBookingFormPage.SelectDeposit(isDeposit);
        }

        [When(@"I select (.*) and (.*) dates")]
        public void WhenISelectAndDates(string checkinDate, string checkoutDate)
        {
            hotelBookingFormPage.FillCheckinDate(checkinDate);
            hotelBookingFormPage.FillCheckoutDate(checkoutDate);
        }

        [Then(@"I click Save button to create new booking")]
        public void ThenIClickSaveButtonToCreateNewBooking()
        {
            hotelBookingFormPage.SaveBooking();
        }

        [Then(@"I click Delete button for the last booking in the table to delete the last booking in the table")]
        public void ThenIClickDeleteButtonForTheLastBookingInTheTableToDeleteTheLastBookingInTheTable()
        {
            WaitExtensions.Wait().Until(ExpectedConditions.ElementExists(By.CssSelector("#bookings .row:last-child")));
            hotelBookingFormPage.DeleteLastBooking();
        }
    }
}
