﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using FluentAssertions;

using static EqualExperts.Hotel.Tests.Base.DriverFactory;

namespace EqualExperts.Hotel.Tests.Pages
{
    public class HotelBookingFormPage
    {
        public IWebElement txtFirstName => Driver.FindElement(By.Id("firstname"));
        public IWebElement txtSurtName => Driver.FindElement(By.Id("lastname"));
        public IWebElement txtPrice => Driver.FindElement(By.Id("totalprice"));
        public IWebElement dpdnDeposit => Driver.FindElement(By.Id("depositpaid"));
        public IWebElement txtCheckin => Driver.FindElement(By.Id("checkin"));
        public IWebElement txtCheckout => Driver.FindElement(By.Id("checkout"));
        public IWebElement btnSave => Driver.FindElement(By.CssSelector("input[value=' Save ']"));
        public IWebElement btnDelete => Driver.FindElement(By.CssSelector("#bookings .row:last-child input[value='Delete']"));
        public IWebElement rowLastBooking => Driver.FindElement(By.CssSelector("#bookings .row:last-child"));

        public void FillFirstName(string firstName)
        {
            txtFirstName.SendKeys(firstName);
        }

        public void FillSurName(string surName)
        {
            txtSurtName.SendKeys(surName);
        }

        public void FillPrice(string price)
        {
            txtPrice.SendKeys(price);
        }

        public void SelectDeposit(string depositDropdownValue)
        {
            var dropdown = new SelectElement(dpdnDeposit);
            dropdown.SelectByText(depositDropdownValue);
        }

        public void FillCheckinDate(string checkinDate)
        {
            txtCheckin.SendKeys(checkinDate);
        }

        public void FillCheckoutDate(string checkoutDate)
        {
            txtCheckout.SendKeys(checkoutDate);
        }

        public void SaveBooking()
        {
            btnSave.Click();
        }

        public void DeleteLastBooking()
        {
            btnDelete.Click();
        }

        public bool IsBookingFormDisplayed()
        {
            txtFirstName.Displayed.Should().BeTrue();
            txtSurtName.Displayed.Should().BeTrue();
            txtPrice.Displayed.Should().BeTrue();
            dpdnDeposit.Displayed.Should().BeTrue();
            txtCheckin.Displayed.Should().BeTrue();
            txtCheckout.Displayed.Should().BeTrue();
            return true;
        }
    }
}
